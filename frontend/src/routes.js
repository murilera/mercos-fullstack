import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Emissao from './pages/Emissao/Emissao';


function Routes({ theme, changeTheme, themeActive }){
  return(
    <BrowserRouter>
      <Switch>
        {<Route path="/" exact component={() => <Emissao theme={theme}/>}/>}
      </Switch>
    </BrowserRouter>
  )
}

export default Routes;