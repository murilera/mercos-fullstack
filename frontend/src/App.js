import React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Routes from './routes';
import { Paper } from '@material-ui/core';
import { useState } from 'react';
import './global.css';


function App(){
  const [darkMode, setDarkMode] = useState(localStorage.getItem("theme") === 'true' ? true : false);

  const theme = createMuiTheme({
    palette:{
      primary:{
        main: darkMode ? '#202024' : '#DCC6B7'
      },
      secondary: {
        main: darkMode? '#364156' : '#5A8C8F'
      },
      background: {
        paper: darkMode? '#121214' : '#f5f5f5'
      },
      text: {
        main: darkMode? '#fff' : '#000'
      }
    }
  });

  const changeTheme = (opt) => {
    setDarkMode(opt);
    localStorage.setItem("theme", opt);
  }


  return (
    <ThemeProvider theme={theme}>
      <Paper elevation={0}>
        <Routes theme={theme} changeTheme={changeTheme} themeActive={darkMode}/>
      </Paper>
    </ThemeProvider>
  );
}

export default App;