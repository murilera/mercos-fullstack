import axios from 'axios';

export const getClients = async () => {
  const response = await axios.get('http://localhost:8080/v1/clients')
  return response
}

export const getProducts = async () => {
  const response = await axios.get('http://localhost:8080/v1/products')
  return response
}

export const insertOrders = async (
    cliente,
    produto,
    quantidade,
    valor_total,
    rentabilidade
  ) => {
  try {
    const response = await axios.post(
      'http://localhost:8080/v1/orders',
      {
        cliente,
        produto,
        quantidade,
        valor_total,
        rentabilidade
      }
    )
    return response.data
  } catch (err) {
    console.error(`Error: ${err}`)
    throw err
  }
}
