import VMasker from 'vanilla-masker';


export function Money (value) {
  return VMasker.toMoney(value, 1234567890)
};

export function Number (value) {
  return VMasker.toNumber(value, '123ac34')
}