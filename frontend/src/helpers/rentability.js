export const nivelRentabilidade = (priceProduct, priceSuggested, quantidade) => {
  const great = 'Ótima';
  const good = 'Boa';
  const bad = 'Ruim';
  
  if (priceProduct === 0 || !quantidade) {
    return ''
  }

  if (priceSuggested === 0 || !priceSuggested) {
    priceSuggested = priceProduct / 100
  }
  priceProduct = priceProduct / 100;
  
     
  const item = (priceProduct * 10) / 100;
  const maximoVenda = priceProduct - item;
  
  if (priceSuggested > priceProduct) return great;
  if (maximoVenda <= priceSuggested) return good;
  return bad;
};

export const quantilyMultiple = (quantity, multiple) => {
  if (+quantity % multiple === 0) return true;
  return false;
};

const exports = { nivelRentabilidade, quantilyMultiple }

export default exports;