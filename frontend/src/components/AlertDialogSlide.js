// import React from 'react';
// import Button from '@material-ui/core/Button';
// import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
// import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
// import DialogTitle from '@material-ui/core/DialogTitle';
// import Slide from '@material-ui/core/Slide';
// import * as controleAPI from '../services/api';


// const Transition = React.forwardRef(function Transition(props, ref) {
//   return <Slide direction="up" ref={ref} {...props} />;
// });

// function AlertDialogSlide(props) {
//   const [open, setOpen] = React.useState(false);

//   const handleClickOpen = () => {
//     setOpen(true);
//   };

//   const handleClose = () => {
//     setOpen(false);
//   };

//   const handleAccept = () => {
//     if (props.info === "Erro Faturamento"){
//       controleAPI.updateFaturamentoPAF(props.address)
//     }
//     else {
//       controleAPI.updateFaturadoPAF(props.address)
//     }
    
//     props.atualizar()
//     setOpen(false);
//   };

//   return (
//     <div style={{textAlign: 'center'}}>
//       <Button style={{color: 'red'}} onClick={handleClickOpen}>
//       CORRIGIR
//       </Button>
//       <Dialog
//         open={open}
//         TransitionComponent={Transition}
//         keepMounted
//         onClose={handleClose}
//         aria-labelledby="alert-dialog-slide-title"
//         aria-describedby="alert-dialog-slide-description"
//       >
        
//           <DialogTitle id="alert-dialog-slide-title"></DialogTitle>
//           <DialogContent>
//             <DialogContentText id="alert-dialog-slide-description">
//             {(props.info === "Erro Faturamento") ? 
//               "Deseja corrigir manualmente este erro? (Status -> Faturamento [PAF])" 
//               :
//               "Deseja corrigir manualmente este erro? (Status -> Faturado [PAF])"
//             }
//             </DialogContentText>
//           </DialogContent>
//           <DialogActions>
//             <Button onClick={handleClose} color="primary" >
//               Cancelar
//             </Button>
//             <Button onClick={handleAccept} color="primary" >
//               Sim
//             </Button>
//           </DialogActions>
//         </Dialog>
//     </div>
//   )

// };

// export default AlertDialogSlide;