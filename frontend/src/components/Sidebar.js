import React, { useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Link, useHistory } from 'react-router-dom';

import { 
  Drawer, 
  CssBaseline, 
  AppBar, 
  Toolbar, 
  List, 
  Typography, 
  Divider,
  IconButton, 
  ListItem, 
  ListItemIcon, 
  ListItemText, 
  Paper, 
  Fab, 
  Grid } from '@material-ui/core';

import { FiMoon, FiSun, FiChevronRight } from 'react-icons/fi';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuIcon from '@material-ui/icons/Menu';
import soma from '../assets/soma.png'

const drawerWidth = 240;

const menuObject = [
  {
      link: '',
      title: 'All'
  },
  // {
  //     link: 'logistic',
  //     title: 'Logística'
  // },
  // {
  //     link: 'embracing',
  //     title: 'Abrangentes'
  // },
  // {
  //     link: 'capacity',
  //     title: 'Capacidade'
  // },
  // {
  //   link: 'dashboard-cf',
  //   title: 'Dashboard Controle Faturamento'
  // }
]

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    background: theme.palette.primary.main,
  },
  drawerPaper: {
    width: drawerWidth,
    background: theme.palette.primary.main,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'space-between'
  },
  content: {
    flexGrow: 1,
    // padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  Fab: {
    border: `1px solid ${theme.palette.secondary.main}`,
    boxShadow: 'none',
    marginRight: '10px',
  },
  ListText: {
    color: theme.palette.text.main
  },
  IconButtonMenu: {
    color: theme.palette.text.main,
    '&:hover': { 
      background: `${theme.palette.secondary.main}`
    },
  },
  Divider: {
    background: theme.palette.text.main
  },
  ListItem: {
    '&:hover': { 
      background: `${theme.palette.secondary.main}`
    },
  }
}));

export default function PersistentDrawerLeft({theme, children, changeTheme, themeActive }) {
  const classes = useStyles();
  const history = useHistory();
  const [open, setOpen] = React.useState(false);

  useEffect(() => {}, [themeActive]);

  const logout = () => {
    localStorage.setItem("token", null);
    localStorage.setItem("name", null);
    history.push('/');
  }

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Paper elevation={0}>
      <AppBar
        color="primary"
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Grid xs={12} style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }} >
            <Toolbar>
            <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
            >
                <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
                Soma Log OMS
            </Typography>            
            </Toolbar>
            {themeActive? 
                (<Fab className={classes.Fab} color="secondary" size="small" aria-label="add" onClick={() => {changeTheme(!themeActive)}}>
                    <FiSun />
                </Fab>)
                :
                (<Fab className={classes.Fab} color="secondary" size="small" aria-label="add" onClick={() => {changeTheme(!themeActive)}}>
                    <FiMoon />
                </Fab>)
            }
        </Grid>
      </AppBar>

      <Drawer
        color="secondary"
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <img src={soma} style={{ width: '90px', marginLeft: '10px' }} alt="logo soma" />
          <IconButton onClick={handleDrawerClose} className={classes.IconButtonMenu}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider className={classes.Divider} />
        <List>
            {menuObject.map(item => {
                return(
                    <Link to={`/${item.link}`} style={{ textDecoration: 'none' }}>
                        <ListItem button key={item.link} className={classes.ListItem}>
                            <ListItemIcon className={classes.ListText}>{<FiChevronRight />}</ListItemIcon>
                            <ListItemText className={classes.ListText} primary={item.title} />
                        </ListItem>
                    </Link>
                );
            })}
        </List>
        {/* <Divider className={classes.Divider}/>
        <List>
            <Link to="/" style={{ textDecoration: 'none' }}>
                <ListItem button key={"logout"} className={classes.ListItem} onClick={(e) => { logout() }}>
                    <ListItemIcon className={classes.ListText}>{<FiChevronRight />}</ListItemIcon>
                    <ListItemText className={classes.ListText} primary={"Sair"} />
                </ListItem>
            </Link>
        </List> */}
      </Drawer>
      </Paper>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />
        {children}
      </main>
    </div>
  );
}