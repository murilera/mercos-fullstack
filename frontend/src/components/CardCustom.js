import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import AlertDialogSlide from './AlertDialogSlide';
import elapsedTime from '../helpers/elapsedTime'

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 280,
    minHeight: 240,
    margin: '5px',
    textAlign: 'center',
    variant: "body1",
    component: "p"
  },
  title: {
    fontSize: 30,
  },
  info: {
    fontSize: 24,
  },
  time: {
    fontSize: 18
  },
  button: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

export default function CardCustom(props) {
  const classes = useStyles();

  const data_status = new Date(props.data_hora)

  let elapsed_time =  elapsedTime(data_status)
  const options = {hour: 'numeric', minute: 'numeric', second: 'numeric'}

  if (props.info === "Faturamento com erro" || props.info === "Em faturamento [PAF]"){

    return (
      <div className="Card">
        <Card className={classes.root} style={{background: props.color}}>
          <CardContent style={{marginTop: "35px"}}>
            <Grid item xs={12}>
                <Typography className={classes.title}>
                {props.title.substring(0,3)} - {props.title.substring(props.title.length -3)}
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Typography className={classes.info}>
                    {props.info}
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Typography className={classes.time}>
                    {elapsed_time}
                </Typography>
            </Grid>

            <AlertDialogSlide address={props.title} info={props.info} atualizar={props.atualizar}/>

          </CardContent>
    
        </Card>
      </div>
    )
  }

  else {
    return (
      <div className="Card">
        <Card className={classes.root} style={{background: props.color}}>
          <CardContent style={{marginTop: "55px"}}>
            <Grid item xs={12}>
                <Typography className={classes.title}>
                {props.title.substring(0,3)} - {props.title.substring(props.title.length -3)}
                </Typography>
            </Grid>
            <Grid xs={12}>
                <Typography className={classes.info}>
                    {props.info}
                </Typography>
            </Grid>
            <Grid xs={12}>
                <Typography className={classes.time}>
                  {elapsed_time}
                </Typography>
            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }

}