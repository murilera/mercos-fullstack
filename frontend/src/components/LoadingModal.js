import React from 'react';

import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import { CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    BoxDialog: {
      color: theme.palette.text.main
    }
  }));

export default function LoadingModal({ open, title, subtitle }) {
    const classes = useStyles();

    return (
        <Dialog
            open={open}
        >
            <DialogTitle>
                <Grid container alignItems={'center'} justify={'center'} >
                    <Grid item className={classes.BoxDialog}>
                        {title}
                    </Grid>
                </Grid>
            </DialogTitle>
            <DialogContent>
                <Grid container justify={'center'}>
                    <Grid item>
                        <CircularProgress className={classes.BoxDialog}></CircularProgress>
                    </Grid>
                </Grid>
            </DialogContent>
            <Grid container alignItems={'center'} justify={'center'} >
                    <Grid item>
                    <Typography className={classes.BoxDialog} variant="subtitle2" gutterBottom>
                        {subtitle ? subtitle : ""}
                    </Typography>
                    </Grid>
                </Grid>
        </Dialog>
    );
}