import {Grid} from '@material-ui/core';
import CardCustom from './CardCustom';

function Selected (props){
  const isSelected = props.isSelected;
  if (isSelected){
    return (
      <div className="MultiSelect">

        <Grid container justify="center" alignItems="center">
          {props.isSelected.map(o => {
            return (
              <Grid>
                <CardCustom title={o.value} info={o.status} color={o.color} data_hora={o.data_hora} atualizar={props.atualizar}/>
              </Grid>
            )
          })}

        </Grid>
      </div>
    )
  }
  return (<div></div>)
}


export default Selected;