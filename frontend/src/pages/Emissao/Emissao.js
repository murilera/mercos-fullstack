import React, { useEffect, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Button } from '@material-ui/core';
import * as api from '../../services/api';
import CustomizedSelects from "./Selection"
import * as mask from '../../helpers/mask'
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import * as rentability from '../../helpers/rentability';
import { toast } from 'react-toastify';


const useStyles = makeStyles((theme) => ({
  sectionNameTitle: {
      color: theme.palette.text.main,
  },
  background: {
      background: theme.palette.background.paper,
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1.5),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  margin: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(2),
    width: '30ch'
  },
}));


function Emissao(props){
  const classes = useStyles(props);
  
  const [ clients, setClients ]  = useState([])
  const [ products, setProducts ]  = useState([])
  const [ lastUpdate, /*setLastUpdate*/ ]  = useState(new Date())
  const [ productName, setProductName ] = useState("")
  const [ productValue, setProductValue] = useState(0)
  const [ clientName, setClientName] = useState("")
  const [ preco, setPreco ] = useState(0)
  const [ quantidade, setQuantidade ] = useState(0)
  const [ valorTotal, setValorTotal ] = useState(0)

  const handleQuantidade = (event) => {
    const quantidade = event.target.value;
    setQuantidade(quantidade)
    
    console.log(quantidade, preco)
    if (preco !== 0) {
      setValorTotal(quantidade*preco)
    }
    setValorTotal(quantidade*productValue)
  }

  const handlePreco = (event) => {
    const preco_update = event.target.value
    setPreco(preco_update)
  }
  
  const handleSubmit = () => {
    const insertOrder = async () => {
      const response = await api.insertOrders(clientName,
        productName,
        quantidade,
        valorTotal,
        rentabilidade);
      const dados = response.data;
      console.log(dados)
    }

    const rentabilidade = rentability.nivelRentabilidade(parseFloat(productValue), parseFloat(preco), quantidade)
  
    if (productName === 'X-Wing') {
      const multiplo = rentability.quantilyMultiple(quantidade, 2)
      if (multiplo) {
        insertOrder()
        toast.success("Sucesso!")
      } else {
        toast.error("Quantidade inválida, apenas multiplos de 2")
      }
    } else if (productName === 'Lightsaber') {
      const multiplo = rentability.quantilyMultiple(quantidade, 5)
      if (multiplo) {
        insertOrder()
        toast.success("Sucesso!")
      } else {
        toast.error("Quantidade inválida, apenas multiplos de 5")
      }
    } else {
      insertOrder()
      toast.success("Sucesso!")
    }
  }

  useEffect(() => {
    const getClients = async () => {
      const response = await api.getClients();
      const dados = response.data;
      setClients(dados)
    }
    getClients()
  }, [lastUpdate])

  useEffect(() => {
    const getProducts = async () => {
      const response = await api.getProducts();
      const dados = response.data;
      setProducts(dados)
    }
   
    getProducts()
  }, [lastUpdate])

  useEffect(() =>{
    if (preco !== 0){
      setValorTotal((preco*quantidade)*100)
    } else {
      setValorTotal(productValue*quantidade)
    }

  },[preco, quantidade, valorTotal, productName, productValue])

  return (
    <div>
      
      <Grid container>
      <Grid item xs={12} sm={6}> 
        <Grid item>
          Emissão de Pedidos
        </Grid>
    
        <Grid item>
          <CustomizedSelects options={clients} title={"Clientes"} setName={setClientName}/>
        </Grid>

        <Grid item>
          <CustomizedSelects options={products} title={"Produtos"} setName={setProductName} setValue={setProductValue}/>
        </Grid>

        <Grid item>
          <FormControl fullWidth className={classes.margin} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-quantity">Quantidade</InputLabel>
            <OutlinedInput
              id="outlined-adornment-quantity"
              value={quantidade}
              onChange={handleQuantidade}
              labelWidth={90}
            />
          </FormControl>
        </Grid>
        
        <Grid item>
          <FormControl fullWidth className={classes.margin} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">Preço (Sugerido)</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              value={mask.Money(productValue)}
              startAdornment={<InputAdornment position="start"></InputAdornment>}
              labelWidth={130}
              disabled={true}
            />
          </FormControl>
        </Grid>

        <Grid item>
          <FormControl fullWidth className={classes.margin} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">Preço (Escolhido)</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              value={preco}
              onChange={handlePreco}
              startAdornment={<InputAdornment position="start"></InputAdornment>}
              labelWidth={130}
            />
          </FormControl>
        </Grid>

      </Grid>
      <Grid item xs={12} sm={6}>
      <Grid item>
        Resumo do Pedido
      </Grid>

      <Grid item>
          <FormControl fullWidth className={classes.margin} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">Cliente</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              startAdornment={<InputAdornment position="start">{clientName}</InputAdornment>}
              labelWidth={60}
              disabled={true}
            />
          </FormControl>
      </Grid>
      <Grid item>
          <FormControl fullWidth className={classes.margin} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">Produto</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              startAdornment={<InputAdornment position="start">{productName}</InputAdornment>}
              labelWidth={60}
              disabled={true}
            />
          </FormControl>
        </Grid>
        <Grid item>
          <FormControl fullWidth className={classes.margin} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">Quantidade</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              startAdornment={<InputAdornment position="start">{mask.Number(quantidade)}</InputAdornment>}
              labelWidth={90}
              disabled={true}
            />
          </FormControl>
        </Grid>
        <Grid item>
          <FormControl fullWidth className={classes.margin} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">Valor Total</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              startAdornment={<InputAdornment position="start">{mask.Money(valorTotal)}</InputAdornment>}
              labelWidth={80}
              disabled={true}
            />
          </FormControl>
        </Grid>
        <Grid item>
          <FormControl fullWidth className={classes.margin} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">Rentabilidade</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              startAdornment={<InputAdornment position="start">{ rentability.nivelRentabilidade(parseFloat(productValue), parseFloat(preco), quantidade) }</InputAdornment>}
              labelWidth={100}
              disabled={true}
            />
          </FormControl>
        </Grid>

        <Grid item> 
          <Button 
            variant="contained"
            className={classes.margin}
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </Grid>

      </Grid>

      </Grid>

    </div>
  );

}


export default Emissao;


