# project - mercos

## installation:

```
npm install
```

## run:

```
npm/yarn start
```

## backend:

1. http://localhost:8080/v1/products (GET) - get products
2. http://localhost:8080/v1/clients (GET) - get clients
3. http://localhost:8080/v1/orders (POST) - send orders - body { cliente: 'string', produto: 'string', quantidade: 'int', valor_total: 'float', rentabilidade: 'string' }
4. http://localhost:8080/v1/populate (POST) - populate database

## frontend:

1. http://localhost:3000

## database:

```
CREATE TABLE `clientes` (
`id` int NOT NULL AUTO_INCREMENT,
`name` varchar(100) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT
```

```
CREATE TABLE `produtos` (
`id` int NOT NULL AUTO_INCREMENT,
`name` varchar(100) DEFAULT NULL,
`value` float DEFAULT NULL,
`multiplier` int DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

```
CREATE TABLE `pedidos` (
`id` float NOT NULL AUTO_INCREMENT,
`cliente` varchar(100) DEFAULT NULL,
`produto` varchar(100) DEFAULT NULL,
`quantidade` int DEFAULT NULL,
`valor_total` decimal(10,2) DEFAULT NULL,
`rentabilidade` varchar(100) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```
