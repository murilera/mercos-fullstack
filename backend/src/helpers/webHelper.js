const asyncHandler = (fn) => (req, res, next) => Promise
  .resolve(fn(req, res, next))
  .catch(next);

const setHeaders = (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  next();
};

const FilteredPaginatedQuery = async (model, filter = [], order = [], pageOpts) => {
  let query = typeof (model) === 'function' ? model.query() : model;

  filter.forEach((f) => {
    query = Array.isArray(f) ? query.where(f[0], f[1], f[2]) : query.where(f);
  });
  order.forEach((o) => {
    query = query.orderBy(o[0], o[1]);
  });

  query = pageOpts.sz != 0 ? query.page(pageOpts.pg || 0, pageOpts.sz || 25) : query;
  let retries = 0;
  while (retries < 100) {
    try {
      return await query.debug();
    } catch (e) {
      retries++;
      query.debug();
      console.error(e);
      console.log('Retrying');
    }
  }
};

const parseSort = (sort) => sort.split(',').map((s) => (s ? [s.slice(1), s[0] == '+' ? 'ASC' : 'DESC'] : undefined)).filter((x) => x);
const parseFilter = (params) => Object.keys(params).flatMap((param) => Object.keys(params[param]).map((op) => [param, switchOp(op), params[param][op]]));

const switchOp = (op) => ({
  eq: '=',
  ne: '<>',
  lt: '<',
  gt: '>',
  gte: '>=',
  lte: '<=',
  in: 'in',
  lk: 'like',
  is: 'is',
}[op] || '=');

export {
  asyncHandler,
  setHeaders,
  FilteredPaginatedQuery,
  parseFilter,
  parseSort,
};
