import express from 'express';
import fs from 'fs';

const app = express.Router();

const getAutoRouted = () => {
  const dir = fs.readdirSync(`${__dirname}/../routes`, { withFileTypes: true });
  return dir
    .map((x) => ({ path: x.name, route: '', file: x.isFile() }))
    .filter((x) => x.path.endsWith('.js') || !x.file)
    .map((x) => {
      if (x.file) {
        const sl = x.path.split('.').slice(0, -1);
        x.path = `../routes/${sl.reduce((a, x) => `${a}.${x}`, '').substr(1)}`;
        x.route = sl.reduce((a, x) => `${a}/${x}`, '');
      } else {
        x.route = `/${x.path}`;
        x.path = `../routes/${x.path}`;
      }
      return x;
    });
};

const routeData = getAutoRouted();

routeData.forEach((x) => {
  const route = require(x.path).default;
  if (typeof route === 'function') {
    app.use(x.route, route);
  }
});

export default app;
