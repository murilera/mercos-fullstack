import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import cookie from 'cookie-parser';
import helmet from 'helmet';
import cors from 'cors';
import AutoRouter from './helpers/autoRoute';

const app = express();

app.server = http.createServer(app);

app.use(bodyParser.json());
app.use(cookie());
app.use(helmet());
app.use(cors());

app.get('/readiness_check', (req, res) => res.send('OK'));
app.get('/liveness_check', (req, res) => res.send('OK'));

app.use('/v1', AutoRouter);

app.server.listen(process.env.PORT);

console.log('Servidor Iniciado!');
