import { Model } from 'objection';
import knex from '../connections/LocalConn';

class Products extends Model {
  static get tableName() {
    return 'produtos';
  }

  static get idColumn() {
    return [];
  }
}
Products.knex(knex);

export default Products;
