import { Model } from 'objection';
import knex from '../connections/LocalConn';

class Clients extends Model {
  static get tableName() {
    return 'clientes';
  }

  static get idColumn() {
    return [];
  }
}
Clients.knex(knex);

export default Clients;
