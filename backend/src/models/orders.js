import { Model } from 'objection';
import knex from '../connections/LocalConn';

class Orders extends Model {
  static get tableName() {
    return 'pedidos';
  }

  static get idColumn() {
    return [];
  }
}
Orders.knex(knex);

export default Orders;
