import K from 'knex';

require('dotenv').config();
const { performance } = require('perf_hooks');

const timings = {};

const knex = K({
  client: process.env.DBCLIENT,
  connection: {
    host: process.env.DBHOST,
    user: process.env.DBUSER,
    password: process.env.DBPASSWORD,
    database: process.env.DBDATABASE,
    connectionTimeout: 10000,
    requestTimeout: 60000,
  },
});

knex.on('query', (query) => {
  const uid = query.__knexQueryUid;
  timings[uid] = performance.now();
}).on('query-response', (response, query) => {
  const uid = query.__knexQueryUid;
  const now = performance.now();
  console.log(`sql: ${query.sql}\nbind: [${query.bindings}]\n${uid}: ${now - timings[uid]}ms`);
  delete timings[uid];
});

export default knex;
