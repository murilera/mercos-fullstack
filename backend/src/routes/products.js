import express from 'express';
import Products from '../models/products';

const router = express.Router();

router.get('/', async (req, res, next) => {
  const query = Products.query();
  const result = await query;

  res.json(result);
});

export default router;
