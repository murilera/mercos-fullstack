import express from 'express';
import Orders from '../models/orders';

const router = express.Router();

router.post('/', async (req, res, next) => {
  const {
    cliente, produto, quantidade, valor_total, rentabilidade,
  } = req.body;

  const data = {
    cliente,
    produto,
    quantidade,
    valor_total,
    rentabilidade,
  };

  const query = Orders.knex().into('pedidos').insert(data);
  const result = await query;

  res.json(result);
});

export default router;
