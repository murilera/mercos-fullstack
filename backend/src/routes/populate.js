import express from 'express';
import Clients from '../models/clients';
import Products from '../models/products';

const router = express.Router();

router.post('/', async (req, res) => {
  const clients = [
    {
      name: 'Darth Vader',
    },
    {
      name: 'Obi-Wan​ ​Kenobi',
    },
    {
      name: 'Luke​ ​Skywalker',
    },
    {
      name: 'Imperador​ ​Palpatine',
    },
    {
      name: 'Han​ ​Solo',
    },
  ];

  const products = [
    {
      name: 'Millenium​ ​Falcon',
      value: 550000.00,
    },
    {
      name: 'X-Wing',
      value: 60000.00,
      multiplier: 2,
    },
    {
      name: 'Super​ ​Star​ ​Destroyer',
      value: 4570000.00,
    },
    {
      name: 'TIE​ ​Fighter',
      value: 75000.00,
      multiplier: 2,
    },
    {
      name: 'Lightsaber',
      value: 6000.00,
      multiplier: 5,
    },
    {
      name: 'DLT-19​ ​Heavy​ ​Blaster​ ​Rifle',
      value: 5800.00,
    },
    {
      name: 'DL-44​ ​Heavy​ ​Blaster​ ​Pistol',
      value: 1500.00,
      multiplier: 10,
    },
  ];

  const queryClients = Clients.knex().into('clientes').insert(clients);
  const resultClients = await queryClients;
  const queryProducts = Products.knex().into('produtos').insert(products);
  const resultProducts = await queryProducts;

  res.json(resultClients, resultProducts);
});

export default router;
