import express from 'express';
import Clients from '../models/clients';

const router = express.Router();

router.get('/', async (req, res, next) => {
  const query = Clients.query();
  const result = await query;

  res.json(result);
});

export default router;
